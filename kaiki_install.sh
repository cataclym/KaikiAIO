#!/bin/sh

root=$(pwd)

# remove old backup
rm -rf KaikiDeishuBot_old 1>/dev/null 2>&1

# make a new backup
mv -fT KaikiDeishuBot KaikiDeishuBot_old 1>/dev/null 2>&1

# clone new version
git clone https://gitlab.com/cataclym/KaikiDeishuBot.git --branch=master --single-branch
wget -q -N https://gitlab.com/cataclym/KaikiAIO/-/raw/main/kaiki_build.sh
bash kaiki_build.sh

cd "$root"
rm "$root/kaiki_install.sh"
exit 0
