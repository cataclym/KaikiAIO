#!/bin/sh

echo "Welcome to KaikiBot installer"
echo ""

root=$(pwd)
choice=9

base_url="https://gitlab.com/cataclym/KaikiAIO/-/raw/main"
script_install="kaiki_install.sh"
script_setup="kaiki_setup.sh"
while [ $choice -eq 9 ]; do

        echo "1. Install Prerequisites"
        echo "2. Download KaikiBot"
        echo "3. Run KaikiBot"
        echo "4. Setup credentials"
        echo "5. Rebuild KaikiBot"
        echo "6. Exit"
        echo -n "Type in the number of an option and press ENTER"
        echo ""
        read choice

        if [ "$choice" -eq 1 ]; then
          echo ""
          echo "This hasn't been implemented yet."
          echo ""
          choice=9

        elif [ "$choice" -eq 2 ]; then
          echo ""
          echo "Downloading the KaikiBot installer script"
          rm "$root/$script_install" 1>/dev/null 2>&1
          wget -N -q "$base_url/$script_install" && bash "$root/$script_install"
          echo ""
          sleep 2s
          choice=9

        elif [ "$choice" -eq 3 ]; then
          echo ""
          echo "Downloading the KaikiBot setup script"
          rm "$root/$script_setup" 1>/dev/null 2>&1
          wget -N -q "$base_url/$script_setup" && bash "$root/$script_setup"
          echo ""
          sleep 2s
          choice=9

        elif [ "$choice" -eq 4 ]; then
          echo ""
          echo "This hasn't been implemented yet."
          echo ""
          choice=9

        elif [ "$choice" -eq 5 ]; then
          echo ""
          echo "This hasn't been implemented yet."
          echo ""
            choice=9

        fi

done
