#!/bin/sh

root=$(pwd)

# Change dir to bot or exit on fail
cd KaikiDeishuBot || exit 1

# Fetch latest tags
git fetch > /dev/null
latestTag=$(git tag | sort -V | tail -1)

# Checkout the latest tag
git checkout "$latestTag"

# Install dependencies & run typescript compiler
npm ci && echo -n "KaikiBot build successfully!"

# Go back
cd "$root" || exit 1

# Copy credentials
cp KaikiDeishuBot_old/.env KaikiDeishuBot/.env 1>/dev/null 2>&1

rm "$root/kaiki_build.sh"
exit 0
