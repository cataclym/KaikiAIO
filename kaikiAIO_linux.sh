#!/bin/sh
echo ""
echo "Welcome to KaikiBot~"
echo "Downloading the latest installer..."
root=$(pwd)

rm "$root/kaiki_menu.sh" 1>/dev/null 2>&1
wget -N -q https://gitlab.com/cataclym/KaikiAIO/-/raw/main/kaiki_menu.sh

bash kaiki_menu.sh
cd "$root" || exit 1
rm "$root/kaiki-menu.sh"
exit 0

