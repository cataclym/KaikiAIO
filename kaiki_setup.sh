#!/bin/sh
root=$(pwd);

read_prefix () {
  read -e -p "Enter your default PREFIX: " -i "," PREFIX;
  check_prefix;
}

check_prefix() {
    if [[ "$PREFIX" == "" ]]; then
      echo "";
      echo "You need to provide a proper prefix!"
      echo "";
      read_prefix;
    fi
}

echo "";
echo "Welcome to KaikiBot setup script";
echo "Read the guide setup <a href='https://gitlab.com/cataclym/KaikiDeishuBot/-/blob/master/docs/ENV.md'>here</a>";
echo "Paste your input using CTRL+SHIFT+V";

read -p "Enter your CLIENT_TOKEN:" CLIENT_TOKEN;
echo "";
echo "CLIENT_TOKEN set to [ $CLIENT_TOKEN ]";
echo "";

read_prefix;
echo "";
echo "PREFIX set to [ $PREFIX ]";
echo "";

read -p "Enter your MySQL DATABASE_URL:" DATABASE_URL;
echo "";
echo "DATABASE_URL set to [ $DATABASE_URL ]";
echo "";

read -e -p "(Optional) Enter your KAWAIIKEY:" KAWAIIKEY;

echo "";
if [[ "$KAWAIIKEY" != "" ]]; then
  echo "KAWAIIKEY set to [ $KAWAIIKEY ]";
else
  echo "KAWAIIKEY commands will be disabled.";
fi
echo "";

cd "$root/KaikiDeishuBot" || exit 1;

strs="CLIENT_TOKEN=$CLIENT_TOKEN
PREFIX=$PREFIX
DATABASE_URL=$DATABASE_URL
KAWAIIKEY=$KAWAIIKEY";

echo "Writing to $root/KaikiDeishuBot/.env"
printf "%s\n" "$strs" >> .env
echo "Done!"
cd "$root" || exit 1;
exit 0;



